build2014091701
- label of ct changed to 'ginger digest'
- removed comments field from front page teasers view
- removed comments field from front page news view
- removed comments field from front page voices view
- views for news and opinion also under article_teasers view

build2014092102
- changed lost found adoption FP views
- to make images in the style of 'node_gallery_thumbnail'
- to show 4 images instead of 3

build2014092101
- views changes
  - items per page increased to 4 pet photos front page view
  - image style changed to node_gallery_file_thumbnail for above

build2014091901
- sort by fb likes
- change titles of all sub-views to 'trending…' instead of 'latest...'
- added views fb like and views periodic execution as dependencies

build2014091802
- replaced type in teasers view with article categories taxonomy term

build2014091801
- made title 'articles' in teasers view
- gave it class=black, h4 and strong 
- new path for article nodes using taxonomy

build2014091602
- remove share this article label
- article - change views to reduce body to 140 uniformly for blog, article and news blocks on front page

build2014091601
- photo gallery view on front page, image size changed to 'teaser'
- lost pet, found pet and adoption front page views made bigger to 165x110
- all the above are sub-views of 'recent articles block' main view

build2014091501 - done on dev3 site 
- comment with open comment box, no ajax
- teasers page - comment made into link in views

7.x-1.0-dev1 - initial commit on sep 12 2014

<?php
/**
 * @file
 * article_plus.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function article_plus_default_rules_configuration() {
  $items = array();
  $items['rules_article_creation_mail_to_editor'] = entity_import('rules_config', '{ "rules_article_creation_mail_to_editor" : {
      "LABEL" : "Article\\/News creation mail to Editor",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : {
        "node_insert--article" : { "bundle" : "article" },
        "node_insert--news" : { "bundle" : "news" }
      },
      "IF" : [
        { "user_has_role" : { "account" : [ "node:author" ], "roles" : { "value" : { "6" : "6" } } } }
      ],
      "DO" : [
        { "node_unpublish" : { "node" : [ "node" ] } },
        { "redirect" : { "url" : "\\u003Cfront\\u003E" } },
        { "drupal_message" : { "message" : "Your submission has been sent to the Editor for approval. You will be notified when it is published." } },
        { "mail" : {
            "to" : "editor@gingertail.in",
            "subject" : "[node:author] has submitted for publication",
            "message" : "hi:\\r\\n\\r\\n[node:author] has submitted \\u0027[node:title]\\u0027 to be considered for publication:\\r\\n\\r\\nYou can visit it at: [node:url]\\r\\n\\r\\nYou can edit it and change its status to \\u0027Published\\u0027 if you decide to.\\r\\n\\r\\n-- Your friendly robot at Gingertail.in",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_article_publication_mail_to_writer'] = entity_import('rules_config', '{ "rules_article_publication_mail_to_writer" : {
      "LABEL" : "Article\\/News publication mail to Writer",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : {
        "node_update--article" : { "bundle" : "article" },
        "node_update--news" : { "bundle" : "news" }
      },
      "IF" : [
        { "data_is" : { "data" : [ "node-unchanged:status" ], "value" : "0" } },
        { "data_is" : { "data" : [ "node:status" ], "value" : "1" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:author" ], "value" : [ "site:current-user" ] } },
        { "mail" : {
            "to" : "[node-unchanged:author:mail]",
            "subject" : "Your submission has been published",
            "message" : "The following submission has been accepted for publication:\\r\\n\\r\\n[node:title]\\r\\n\\r\\nYou can visit it by going here: [node:url]\\r\\n\\r\\n-- Editor at Gingertail.in",
            "from" : "Editor@gingertail.in",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_create_photo_node_from_contest_entry'] = entity_import('rules_config', '{ "rules_create_photo_node_from_contest_entry" : {
      "LABEL" : "Create photo node from contest entry",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "DO" : []
    }
  }');
  $items['rules_report_abuse_to_site_admin'] = entity_import('rules_config', '{ "rules_report_abuse_to_site_admin" : {
      "LABEL" : "Report abuse to site admin",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "flag", "rules" ],
      "ON" : { "flag_flagged_report_abuse" : [] },
      "IF" : [
        { "flag_threshold_comment" : {
            "flag" : "report_abuse",
            "comment" : [ "flagged-comment" ],
            "number" : "1",
            "operator" : "\\u003E="
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "webmaster@gingertail.in",
            "subject" : "[flag:title] flag has been triggered",
            "message" : "[flag:title] flag has been triggered on the following comment:\\r\\n\\r\\n[flagged-comment:body]\\r\\n\\r\\nThis has been reported as abuse by:\\r\\n\\r\\n[flagged-comment:flag-report-abuse-user]\\r\\n\\r\\nYou can visit the node at [flagged-comment:url]\\r\\n\\r\\n-- Your friendly robot at gingertail.in",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}

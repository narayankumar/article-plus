<?php
/**
 * @file
 * article_plus.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function article_plus_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'other',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '046969e8-b5ac-44b7-9a8c-f64568b43310',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'World Pet Day',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '05e63d9d-c2e3-4899-a12e-c66eb98286c8',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'vet',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '0b7a79f4-35f8-4123-87b1-e056f23e9efd',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Pet Selection',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '0ba572d0-9441-49f6-9770-88d5cc662598',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Story',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => '1256bf54-f9d7-4b33-a48a-0e5538377826',
    'vocabulary_machine_name' => 'voices_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'batra',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '172869a4-f870-496f-838b-ef74d751c304',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'food',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '20826f05-ca44-4b1e-9928-7b3731beed56',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'dogs',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '2a1d488c-e450-4c82-9610-d34849a6a80b',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Celebrations',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '2ac47a1b-e830-4498-a283-92ffb5d79989',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Cat',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '2ced31f1-1438-4f27-b8ae-8d44a4039d24',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'ExpertSpeak',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '2ec167db-f583-41a6-8872-abdfb060fc79',
    'vocabulary_machine_name' => 'voices_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'smart',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '314687c0-1156-4df7-91aa-92ad912ad568',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Pets',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '319a2fcc-6a27-4000-9229-b14a93ff2a3e',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Opinion',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '380d7f78-b4dc-4e41-8004-2b1297f88fb4',
    'vocabulary_machine_name' => 'voices_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Editorial',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => '3ad35b24-4299-446e-ab76-923a3b47cba0',
    'vocabulary_machine_name' => 'voices_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'nothing',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '3f4cbbdb-7b28-4449-afed-40963969f7eb',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Training',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '418a51c4-b884-4dfe-a7e4-f93fa58cc887',
    'vocabulary_machine_name' => 'article_categories',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'indian breed',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '42497284-e969-431a-8bc1-d285e782948d',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'cage',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '44256e3f-b459-458f-90fd-eb414bc51684',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Show',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '4548b920-b237-4a95-8739-713a1adf498d',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Hamster',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '581dca69-1fc5-41b0-a9a6-1d5e0068958b',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'People n Pets',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '5e44cc12-ac44-4d22-9d42-128e69012abb',
    'vocabulary_machine_name' => 'article_categories',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Rescue',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '675d0983-7242-4ad3-b04d-f9696c760fd3',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Animal Rights',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '6ed56100-3b5d-4d0f-b28c-d7d9085ed6ce',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Articles',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '717a3406-1da8-449c-9ccf-1e01e5428226',
    'vocabulary_machine_name' => 'magazine_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Health',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '75987c5a-b523-4e4e-9d2d-c1d30353483f',
    'vocabulary_machine_name' => 'article_categories',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'dog videos',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7a08f2a9-ccb4-4a4c-a58d-cf3dee2c4010',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'funny video',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7ab32181-476f-4ed3-a330-d51e6d369613',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'that',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7c87cb63-d5d0-4629-8f38-914db8cafaae',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Violence',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8318a21a-cc54-4516-b529-4885310a8a79',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'this',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '88a98302-9849-4309-9860-29b71d509618',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Stray',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8e7d5623-bfb8-4851-9305-1a536c8bbd99',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'test',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8f3d05fb-9efa-4bed-8a99-f1bfcec989c0',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Cats',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '90d3dc1c-faaf-4592-bc42-f86f7b1520eb',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'NGO',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '98d42218-3e8e-4f6c-b47f-61e68d7d12ae',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Health',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'a2c2547d-4080-455a-b85a-bb96d2934ef3',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Voices',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'a4959afc-cdec-4f57-982c-355703757928',
    'vocabulary_machine_name' => 'magazine_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'chocolate',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'a6eb7ea2-d21e-4033-8c5c-039090cc2146',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Dog',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'a8459dd1-28b7-4a5f-9b87-e63fc4be0324',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Animal Lovers',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b0edeee7-e7b0-465e-b9da-670c46ca12a2',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Safety',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'bad53ec4-690c-4bf9-9d12-55948099cbf2',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Sign Language',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'c1ba5ced-a400-4ade-bcd1-ecc0999823ba',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Babies',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'c8f0b8f3-017a-4d87-a4aa-4c1c38093e87',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'dog care',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'cdfe4db8-ca86-4746-a6a9-f4c4419dd896',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'alpha',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd51cdeff-c893-4b3c-99b3-59e345e4c068',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Allergies',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'e1bc8b9f-dda7-4efd-88fa-71b3d4478e31',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'theta',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'ee696270-da2a-42e1-8291-313c4c6e7ea3',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'birds',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'ee821235-7ed3-41a2-9acf-ee90e480b5f6',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Grooming',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => 'f3b55e56-6787-45d7-89b3-78c49fe32c6d',
    'vocabulary_machine_name' => 'article_categories',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Event',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'fddbc9d9-f572-4486-a727-441c95fc4c3a',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'poisoning',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'fec50213-f522-4499-a22a-e7a7eeb9b1f1',
    'vocabulary_machine_name' => 'tags',
    'metatags' => array(),
  );
  return $terms;
}

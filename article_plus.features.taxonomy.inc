<?php
/**
 * @file
 * article_plus.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function article_plus_taxonomy_default_vocabularies() {
  return array(
    'article_categories' => array(
      'name' => 'Article categories',
      'machine_name' => 'article_categories',
      'description' => 'Taxonomy for Articles',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'magazine_category' => array(
      'name' => 'Magazine Category',
      'machine_name' => 'magazine_category',
      'description' => 'Taxonomy to assign CTs to different menus',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'tags' => array(
      'name' => 'Tags',
      'machine_name' => 'tags',
      'description' => 'Use tags to group articles on similar topics into categories.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'voices_category' => array(
      'name' => 'Voices Category',
      'machine_name' => 'voices_category',
      'description' => 'Taxonomy to assign CTs to Voice Menu',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}

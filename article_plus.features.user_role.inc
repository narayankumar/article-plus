<?php
/**
 * @file
 * article_plus.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function article_plus_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  // Exported role: directory user.
  $roles['directory user'] = array(
    'name' => 'directory user',
    'weight' => 6,
  );

  // Exported role: editor.
  $roles['editor'] = array(
    'name' => 'editor',
    'weight' => 4,
  );

  // Exported role: forum monitor.
  $roles['forum monitor'] = array(
    'name' => 'forum monitor',
    'weight' => 8,
  );

  // Exported role: registered user.
  $roles['registered user'] = array(
    'name' => 'registered user',
    'weight' => 9,
  );

  // Exported role: retailer.
  $roles['retailer'] = array(
    'name' => 'retailer',
    'weight' => 7,
  );

  // Exported role: site admin.
  $roles['site admin'] = array(
    'name' => 'site admin',
    'weight' => 3,
  );

  // Exported role: writer.
  $roles['writer'] = array(
    'name' => 'writer',
    'weight' => 5,
  );

  return $roles;
}
